#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	Point b(length, width);
	b += a;
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_length = length;
	this->_width = width;
}

myShapes::Rectangle::~Rectangle()
{
	this->_points.clear();
	this->_length = 0;
	this->_width = 0;
}

double myShapes::Rectangle::getArea() const
{
	return _length * _width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return std::sqrt(_length * _length + _width * _width) * MUL_BY_2; // Perimeter formula
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::move(const Point& other)
{
	this->_points[0] += other;
	this->_points[1] += other;
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}
