#pragma once
#include "Polygon.h"
#include <math.h>

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();

	// override functions
	virtual double getArea() const;
	virtual double getPerimeter() const;

	virtual void draw(const Canvas& canvas);
	virtual void move(const Point& other);

	virtual void clearDraw(const Canvas& canvas);

private:
	std::vector<Point> _points;
};