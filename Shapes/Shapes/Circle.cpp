#include "Circle.h"

Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(center)
{
	this->_radius = radius;
}

Circle::~Circle()
{
	this->_radius = 0;
}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

double Circle::getArea() const
{
	return PI * _radius * _radius; // area calc
}

double Circle::getPerimeter() const
{
	return PI * MUL_BY_2 * _radius; // perimeter calc
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::move(const Point& other)
{
	this->_center += other;
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


