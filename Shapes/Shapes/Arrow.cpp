#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}

Arrow::~Arrow()
{
	this->_points.clear();
}

double Arrow::getArea() const
{
	return 0.0;
}

double Arrow::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]); // perimeter is the distance between 2 points
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

void Arrow::move(const Point& other)
{
	this->_points[0] += other;
	this->_points[1] += other;
}

void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

