#include "Polygon.h"
#include <string>

#define DIVIDE_BY_2 2

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();

	// override functions
	virtual double getArea() const;
	virtual double getPerimeter() const;

	virtual void draw(const Canvas& canvas);
	virtual void move(const Point& other);

	virtual void clearDraw(const Canvas& canvas);
};