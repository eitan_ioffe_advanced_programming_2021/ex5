#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"
#include <vector>

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3

#define DRAW_SHAPE 0
#define MOD_SHAPE 1
#define DELETE_SHAPES 2
#define EXIT 3

#define MOVE 0
#define DETAILS 1
#define REMOVE_SHAPE 2

#define NUM_OF_SHAPES 3

class Menu
{
public:

	Menu();
	~Menu();

	int getChoice() const;
	bool isEmpty() const; // returns if there are shapes in the vector

	void mainMenu();
	void checkInput(const int min, const int max);
	Point initPoint() const;
	void drawShape();
	void modShape();
	void deleteAllShapes();

private: 
	std::vector <Shape*> _shapes;
	Canvas _canvas;
	int _choice;
};

