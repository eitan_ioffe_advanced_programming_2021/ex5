#include "Shape.h"

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{
	this->_name = "";
	this->_type = "";
}

void Shape::printDetails() const
{
	std::cout << "Name: " << this->_name << std::endl;
	std::cout << "Type: " << this->_type << std::endl;
	std::cout << "Perimeter: " << this->getPerimeter() << std::endl;
	std::cout << "Area: " << this->getArea() << std::endl;
}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}
