#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

Triangle::~Triangle()
{
	this->_points.clear();
}

double Triangle::getArea() const
{
	double s = this->getPerimeter() / DIVIDE_BY_2;
	return std::sqrt(s * (s - _points[A_POINT].distance(_points[B_POINT])) * (s - _points[B_POINT].distance(_points[C_POINT])) * (s - _points[A_POINT].distance(_points[C_POINT]))); // Heron's formula
}

double Triangle::getPerimeter() const
{
	return _points[A_POINT].distance(_points[B_POINT]) + _points[B_POINT].distance(_points[C_POINT]) + _points[A_POINT].distance(_points[C_POINT]); // adding all 3 lines
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[A_POINT], _points[B_POINT], _points[C_POINT]);
}

void Triangle::move(const Point& other)
{
	this->_points[A_POINT] += other;
	this->_points[B_POINT] += other;
	this->_points[C_POINT] += other;
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[A_POINT], _points[B_POINT], _points[C_POINT]);
}
