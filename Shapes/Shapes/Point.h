#pragma once

#include <iostream>
#include <math.h>

#define SQRT 2

class Point
{
public:
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();
	
	Point operator+(const Point& other) const; // returns a point with addition of 2 points
	Point& operator+=(const Point& other); // returns *this affter adding other's values to it

	double getX() const;
	double getY() const;

	double distance(const Point& other) const; // calculating distance

private:
	double _x;
	double _y;
};