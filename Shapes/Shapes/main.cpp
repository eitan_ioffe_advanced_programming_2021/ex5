#include "Menu.h"


int main()
{
	Menu* menu = new Menu();

	menu->mainMenu(); // printing main menu and getting choice
	while (menu->getChoice() != EXIT)
	{
		switch (menu->getChoice())
		{
		case DRAW_SHAPE:
		{ // drawing shape
			menu->drawShape();
		};
		break;
		case MOD_SHAPE:
		{
			if (!menu->isEmpty()) // if not empty
			{
				menu->modShape();
			}
			else {
				std::cout << "No shapes created" << std::endl;
			}
		};
		break;
		case DELETE_SHAPES:
		{
			menu->deleteAllShapes();
		};
		break;
		}
		menu->mainMenu();
	}

	delete (menu); // deleting memory

	return 0;
}