#pragma once

#include "Shape.h"
#include "Point.h"
#include "Polygon.h"

#define PI 3.14
#define MUL_BY_2 2

class Circle : public Shape
{
public:
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);
	~Circle();

	const Point& getCenter() const;
	double getRadius() const;

	// override functions
	virtual double getArea() const;
	virtual double getPerimeter() const;

	virtual void draw(const Canvas& canvas);
	virtual void move(const Point& other);

	virtual void clearDraw(const Canvas& canvas);

private:
	double _radius;
	Point _center;
};