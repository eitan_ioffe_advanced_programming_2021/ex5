#include "Menu.h"

Menu::Menu() 
{
	this->_choice = 0;
}

Menu::~Menu()
{
	deleteAllShapes(); // deleting shapes
	this->_choice = 0;
}

int Menu::getChoice() const
{
	return this->_choice;
}

bool Menu::isEmpty() const
{
	return !bool(_shapes.size());
}

void Menu::mainMenu()
{
	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
	checkInput(0, NUM_OF_SHAPES); // if input is illegal
}

void Menu::checkInput(const int min, const int max)
{
	std::cin >> _choice;
	getchar(); // clean buffer
	while (_choice < min || _choice > max)
	{
		std::cout << "Try again." << std::endl;
		std::cin >> _choice;
		getchar(); // clean buffer
	}
}

Point Menu::initPoint() const
{
	double x = 0, y = 0;

	// Initializing
	std::cout << "Please enter X:" << std::endl;
	std::cin >> x;
	getchar();
	std::cout << "Please enter Y:" << std::endl;
	std::cin >> y;
	getchar();
	Point p(x, y);

	return p;
}

void Menu::drawShape()
{
	double radius = 0, length = 0, width = 0;
	std::string name = "";

	// Choosing the shape to draw
	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
	checkInput(0, NUM_OF_SHAPES);

	switch (_choice)
	{
	case CIRCLE:
	{
		Point p(initPoint()); // getting center
		std::cout << "Enter radius:" << std::endl;
		std::cin >> radius;
		getchar();
		if (radius <= 0) { // in case radius is illegal
			std::cerr << "Radius has to be possitive" << std::endl;
			break;
		}
		std::cout << "Enter circle name:" << std::endl;
		getline(std::cin, name);

		Circle* circle = new Circle(p, radius, "Circle", name); // creating circle
		this->_shapes.push_back(circle); // adding to vector
		circle->draw(_canvas);
	};
	break;

	case ARROW:
	{
		Point p1(initPoint()); // getting 2 points
		Point p2(initPoint());

		std::cout << "Enter arrow name:" << std::endl;
		getline(std::cin, name);

		Arrow* arrow = new Arrow(p1, p2, "Arrow", name); // creating arrow
		this->_shapes.push_back(arrow); // adding to vector
		arrow->draw(_canvas);
	};
	break;

	case TRIANGLE:
	{
		Point p1(initPoint()); // getting 3 points of the triangle
		Point p2(initPoint());
		Point p3(initPoint());

		std::cout << "Enter triangle name:" << std::endl;
		getline(std::cin, name);

		Triangle* triangle = new Triangle(p1, p2, p3, "Triangle", name); // creating triangle

		if (triangle->getArea() == 0) // in case all points are on the same line
		{
			delete (triangle); // deleting triangle
			std::cerr << "Not a triangle" << std::endl;
			break;
		}

		this->_shapes.push_back(triangle); // adding to vector
		triangle->draw(_canvas);
	};
	break;

	case RECTANGLE:
	{
		std::cout << "Enter left corner values:" << std::endl;
		Point p(initPoint());
		std::cout << "Please enter the length of the shape:" << std::endl;
		std::cin >> length;
		std::cout << "Please enter the width of the shape:" << std::endl;
		std::cin >> width;
		getchar();

		if (length <= 0 || width <= 0) { // checking if values are illegal
			std::cerr << "Width or length must be positive" << std::endl;
			break;
		}

		std::cout << "Enter rectangle name:" << std::endl;
		getline(std::cin, name);

		myShapes::Rectangle* rectangle = new myShapes::Rectangle(p, length, width, "Rectangle", name); // creating shape
		this->_shapes.push_back(rectangle); // adding to vector
		rectangle->draw(_canvas);
	};
	break;
	}
}

void Menu::modShape()
{
	int index = 0;
	for (unsigned int i = 0; i < _shapes.size(); i++) // displaying current shapes avilable
	{
		std::cout << "Enter " << i << " for "<< _shapes[i]->getName() << " (" << _shapes[i]->getType() << ")" << std::endl;
	}
	checkInput(0, _shapes.size() - 1);
	index = _choice; // saving chosen shape index

	// chosing function
	std::cout << "Enter 0 to move the shape" << std::endl;
	std::cout << "Enter 1 to get its details" << std::endl;
	std::cout << "Enter 2 to remove the shape" << std::endl;
	checkInput(0, DELETE_SHAPES);

	switch (_choice)
	{
	case MOVE:
	{
		std::cout << "Please enter the X and Y moving scale:" << std::endl;
		Point p(initPoint());

		_shapes[index]->clearDraw(_canvas); // removing draw
		_shapes[index]->move(p); // moving points
		_shapes[index]->draw(_canvas); // drawing again
	}
	break;

	case DETAILS:
	{
		_shapes[index]->printDetails();
	}
	break;

	case REMOVE_SHAPE:
	{
		_shapes[index]->clearDraw(_canvas); // removing draw
		delete (_shapes[index]); // deleting memory
		_shapes.erase(_shapes.begin() + index); // removing from vector

		for (unsigned int i = 0; i < _shapes.size(); i++) // drawing all shapes again
		{
			_shapes[i]->draw(_canvas);
		}
	}
	break;
	}
}

void Menu::deleteAllShapes()
{
	for (unsigned int i = 0; i < _shapes.size(); i++)
	{ // removing shape one by 1
		_shapes[i]->clearDraw(_canvas);
		delete (_shapes[i]);
	}
	_shapes.clear(); // clear vector
}
