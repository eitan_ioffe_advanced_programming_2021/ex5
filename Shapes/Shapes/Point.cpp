#include "Point.h"

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point Point::operator+(const Point& other) const
{
	Point point(*this);
	point += other;

	return point;
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;

	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	double dis = std::sqrt(std::pow(this->_x - other._x, SQRT) + std::pow(this->_y - other._y, SQRT)); // using pythagoras formula
	return dis;
}
